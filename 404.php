<?php get_header(); ?>

    <div class="row">

        <div class="col-xs-12">
            <!-- English version -->
            <h1><?php _e( "Error 404", '' ); ?></h1>
            <p><?php _e( "We couldn't find the page you were looking for. This is either because:", '' ); ?></p>

            <ul>
                <li><?php _e( "There is an error in the URL you entered into your web browser. Please check the URL and try again.", '' ); ?></li>
                <li><?php _e( "The page you are looking for has been moved or deleted.", '' ); ?></li>
            </ul>
            <p><?php printf( __( "You can return to our homepage by %sclicking here%s.", '' ), '<a href="' . site_url() . '">', '</a>' ); ?></p>

            <!-- Slovene version -->
            <h1><?php _e( "Napaka 404", '' ); ?></h1>
            <p><?php _e( "Iskane strani ni bilo mogoče najti. To je lahko iz več razlogov:", '' ); ?></p>

            <ul>
                <li><?php _e( "V URL naslovu je prišlo do tipkarske napake. V tem primeru preverite črkovanje URL-ja in poiskusite ponovno.", '' ); ?></li>
                <li><?php _e( "Stran, ki jo iščete je bila ali premakjena ali izbrisana.", '' ); ?></li>
            </ul>
            <p><?php printf( __( "Priporočamo, da greste na %sdomačo stran%s in nadaljujete tam.", '' ), '<a href="' . site_url() . '">', '</a>' ); ?></p>
        </div>


    </div>


<?php get_footer(); ?>