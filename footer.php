</div> <!-- close main container -->
<footer class="container">
    <div class="row">
        <div class="col-xs-12 authorship">
            <?php printf( __( "Design and development by %sZ-IP%s. &copy; 2015 ZIP.", '' ), '<a href="http://z-ip.eu" title="Združba IP, d.o.o.">', '</a>' ); ?>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>