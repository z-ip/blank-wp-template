<?php
/**
 * ZIP Wordpress + Bootstrap template
 *
 * Includes social integrations: Facebook, Instagram, Twitter, Mailchimp & Share buttons
 * @todo    Twitter
 *
 * @version 1.4
 * @author  Gregor Sušanj, Združba IP, d.o.o.
 * @link    http://z-ip.eu
 */

// TODO: Check correct locale
setlocale( LC_ALL, 'sl_SI' );
date_default_timezone_set( 'Europe/Ljubljana' );

include_once "libraries/classThemeSettings.php";
require_once "libraries/classEntry.php";
require_once "libraries/social.php";
require_once "libraries/social-variables.php";

include_once "libraries/cptExample.php";


new ZIP_Template( "1.0.4" );


/**
 * Class used to define action and filters for theme requirments.
 */
class ZIP_Template
{
    /**
     * @var string Used to save theme version information.
     */
    public $ver;

    /**
     * Basic class used to setup actions and filters for template usage.
     *
     * @param $ver string used to set theme version
     */
    function __construct( $ver )
    {
        $this->ver = $ver;

        add_action( 'after_switch_theme', array( $this, 'action_theme_activation' ) );
        add_action( 'switch_theme',  array( $this, 'action_theme_deactivation' ) );


        add_action( 'wp_enqueue_scripts', array( $this, 'action_enqueue_scripts' ) );
        add_action( 'init', array( $this, "action_init" ) );

        if ( is_admin() ) {
            add_action( 'admin_init', array( $this, 'action_admin_init' ) );
            add_action( 'admin_menu', array( $this, 'action_remove_menus' ) );
            // TODO: If using CPT:
            //add_action( 'add_meta_boxes', array( $this, 'action_add_meta_boxes' ) );
            //add_action( 'save_post_CPT_name', array( $this, 'action_save_post_CPT_name' ) );
        } else {
            add_filter( 'pre_get_posts', array( $this, 'filter_pre_get_posts' ) );
        }
    }

    /**
     * Init specfic administration tasks
     */
    public function admin_init()
    {
        // Add custom TinyMCE styling, based on sitewide styling
        add_editor_style( 'css/custom-editor.min.css' );

        add_action( 'admin_enqueue_scripts', array( $this, 'action_admin_enqueue_scripts' ) );

        //add_action( 'manage_posts_custom_column', array( $this, 'action_custom_columns' ) );
        //add_filter( 'manage_edit-CPT_name_columns', array( $this, 'filter_display_custom_gallery_columns' ) );

    }

    /**
     * 'after_switch_theme' action call, runs during theme activation
     */
    public function action_theme_activation()
    {
        flush_rewrite_rules();
    }

    /**
     * 'switch_theme' action call, runs during theme deactivation
     */
    public function action_theme_deactivation()
    {
        flush_rewrite_rules();
    }


    /**
     * wp_enqueue_scripts action call.
     * Used to set styles and scripts for template usage.
     */
    public function action_enqueue_scripts()
    {
        wp_enqueue_style( 'fortawsome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', array() );

        /*// Bootstrap 3:
        wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css', array() );
        wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js', array( 'jquery' ) );*/
        // Bootstrap 4:
        wp_enqueue_script( 'tether', 'https://cdnjs.cloudflare.com/ajax/libs/tether/1.1.1/js/tether.min.js', array( 'jquery' ) );
        wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(
            'tether',
            'jquery'
        ) );
        wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array() );

        if ( WP_DEBUG ) {
            wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/style.css', array(
                'fortawsome',
                'bootstrap'
            ), $this->ver );
            wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), $this->ver );
        } else {
            wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/style.min.css', array(
                'fortawsome',
                'bootstrap'
            ), $this->ver );
            wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/js/script.min.js', array( 'jquery' ), $this->ver );
        }
    }


    /**
     * admin_enqueue_scripts action call.
     * Used to load custom styles and scripts in administration area
     */
    public function action_admin_enqueue_scripts()
    {
        wp_enqueue_style( 'custom-admin-style', get_template_directory_uri() . '/css/admin_style.min.css', array(), $this->ver );
        wp_enqueue_script( 'custom-admin-script', get_template_directory_uri() . '/js/admin_script.min.js', array( 'jquery' ), $this->ver );
    }


    /**
     * Register custom post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     */
    public function action_init()
    {

    }


    /**
     * Remove menus from administration. Used to make it nicer for the end user.
     *
     * @todo: Uncomment unused, usually atleast Tools & Commments
     */
    public function action_remove_menus()
    {

        //remove_menu_page( 'index.php' );                  //Dashboard
        //remove_menu_page( 'edit.php' );                   //Posts
        //remove_menu_page( 'upload.php' );                 //Media
        //remove_menu_page( 'edit.php?post_type=page' );    //Pages
        //remove_menu_page( 'edit-comments.php' );          //Comments
        //remove_menu_page( 'themes.php' );                 //Appearance
        //remove_menu_page( 'plugins.php' );                //Plugins
        //remove_menu_page( 'users.php' );                  //Users
        //remove_menu_page( 'tools.php' );                  //Tools
        //remove_menu_page( 'options-general.php' );        //Settings

        //remove_submenu_page('wpcf7', 'wpcf7-integration');  // Contact form 7

    }

    /**
     * Filter called just before the WP_Query is exceuted. Enables Query modification under certain curcumstances.
     *      Additional feature description.
     *
     * @param $query WP_Query The Query.
     */
    public function filter_pre_get_posts( $query )
    {
        if ( ! is_admin() && $query->is_main_query() ) {
        }
    }
}