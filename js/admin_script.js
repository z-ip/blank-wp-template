(function($) {
    "use strict";
    // Inside of this function, $() will work as an alias for jQuery()
    // and other libraries also using $ will not be accessible under this shortcut
    // On file load


})(jQuery);

jQuery(document).ready(function($) {
    "use strict";
    // Inside of this function, $() will work as an alias for jQuery()
    // and other libraries also using $ will not be accessible under this shortcut
    // On document ready


});
