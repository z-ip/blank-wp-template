<?php

/**
 * Class Entry
 */
class Entry
{
    public $ID;
    public $post_title;
    public $post_content;
    public $post_name;
    public $post_date;
    public $post_status;
    public $post_type;
    public $post_author;
    public $thumbnail_id;
    public $thumbnail;
    public $categories;
    public $permalink;
    public $meta;

    const META_PREFIX = "_meta_";


    /**
     * Extended WP_Post used to manipulate objects in template
     *
     * @param int|WP_Post WP_Post ID or object.
     */
    function __construct( $arg )
    {
        if ( is_int( $arg ) ) {
            $o = get_post( $arg );
        } else {
            $o = $arg;
        }
        $this->ID           = $o->ID;
        $this->post_title   = $o->post_title;
        $this->post_content = $o->post_content;
        $this->post_name    = $o->post_name;
        $this->post_date    = $o->post_date;
        $this->post_status  = $o->post_status;
        $this->post_type    = $o->post_type;
        $this->post_author  = get_the_author_meta( 'user_nicename', $o->post_author );
        $this->permalink    = get_the_permalink( $this->ID );

        if ( has_post_thumbnail( $this->ID ) ) {
            $this->thumbnail_id = get_post_thumbnail_id( $this->ID );
            $this->thumbnail    = wp_get_attachment_image_src( $this->thumbnail_id, "large" );
        } else {
// TODO: Add default image
            $this->thumbnail_id = "";
            $this->thumbnail    = get_template_directory_uri() . "/img/";
        }

        $taxonomy_names = get_post_taxonomies( $o );
        foreach ( $taxonomy_names as $name ) {
            $this->categories[ $name ] = wp_get_post_terms( $this->ID, $name );
        }

        $this->meta = get_post_meta( $this->ID );
    }

    /**
     * Echo filtered Post Title
     */
    public function the_title()
    {
        echo $this->get_title();
    }

    /**
     * Get filtered Post Title
     *
     * @return string The Title
     */
    public function get_title()
    {
        return apply_filters( 'the_title', $this->post_title );
    }

    /**
     * Echo escaped permalink
     */
    public function the_permalink()
    {
        echo esc_url( $this->permalink );
    }

    /**
     * Echo filtered Post Content
     */
    public function the_content()
    {
        echo $this->get_content();
    }

    /**
     * Get filtered Post Content
     *
     * @return string The Content
     */
    public function get_content()
    {
        return apply_filters( 'the_content', $this->post_content );
    }

    /**
     * Echo 30 word long Post Excerpt
     */
    public function the_excerpt()
    {
        echo $this->get_excerpt();
    }

    /**
     * Get 30 word long Post Excerpt
     *
     * @return string The Excerpt
     */
    public function get_excerpt()
    {
        // TODO: Set trim length
        return wp_trim_words( $this->post_content, 30, '&hellip;' );
    }

    /**
     * Echo the thumbnail uri
     */
    public function the_image_url()
    {
        echo $this->get_image_url();
    }

    /**
     * Get the thumgbnail url
     *
     * @return string The Thumbnail Url
     */
    public function get_image_url()
    {
        return $this->thumbnail[0];
    }

    /**
     * Checks if post meta exists. By key.
     *
     * @param $key   string The key.
     * @param $index int (optional) The meta data index. Default is first element (index 0).
     *
     * @return bool
     */
    public function has_meta_data( $key, $index = 0 )
    {
        return ! empty( $this->get_meta_data( $key, $index ));
    }

    /**
     * Echo post meta by key.
     *
     * @param $key   string The key.
     * @param $index int (optional) The meta data index. Default is first element (index 0).
     */
    public function the_meta_data( $key, $index = 0 )
    {
        echo $this->get_meta_data( $key, $index );
    }

    /**
     * Get post meta by key.
     *
     * @param $key   string The key.
     * @param $index int (optional) The meta data index. Default is first element (index 0).
     *
     * @return string|array The return value.
     */
    public function get_meta_data( $key, $index = 0 )
    {
        if ( ! empty( $this->meta ) && isset( $this->meta[ $key ] ) ) {
            return $this->meta[ $key ][ $index ];
        } else {
            return "";
        }
    }

    /**
     * Checks if post meta exists. By key.
     *
     * @param $key   string The key.
     * @param $index int (optional) The meta data index. Default is first element (index 0).
     *
     * @return bool
     */
    public function has_meta( $key, $index = 0 )
    {
        return ! empty( $this->get_meta( $key, $index ));
    }

    /**
     * Echo post meta by key.
     *
     * @param $key   string The key.
     * @param $index int (optional) The meta data index. Default is first element (index 0).
     */
    public function the_meta( $key, $index = 0 )
    {
        echo $this->get_meta( $key, $index );
    }

    /**
     * Get post meta by key.
     *
     * @param $key   string The key.
     * @param $index int (optional) The meta data index. Default is first element (index 0).
     *
     * @return string|array The return value.
     */
    public function get_meta( $key, $index = 0 )
    {
        $constructed_key = static::META_PREFIX . $key;
        if ( ! empty( $this->meta ) && isset( $this->meta[ $constructed_key ] ) ) {
            return $this->meta[ $constructed_key ][ $index ];
        } else {
            return "";
        }
    }

    /**
     * Print properly contained social links for sharing.
     *
     * @param array $type The Social network to print. Available: pinterest, facebook, instagram, twitter, linkedin,
     *                    gplus, tumblr or all.
     */
    public function print_social_icons( $type = array() )
    {
        if ( function_exists( "get_social_share" ) && function_exists( "the_social_share" ) && ! empty( $type ) ) {
            if ( $type[0] == "any" ) {
                $type = array( "pinterest", "facebook", "instagram", "twitter", "linkedin", "gplus", "tumblr" );
            }
            foreach ( $type as $t ) {
                $arg = array(
                    'type'      => $t,
                    'url'       => $this->permalink,
                    'image_url' => $this->get_image_url(),
                    'title'     => $this->get_title(),
                    'text'      => $this->get_excerpt(),
                    'source'    => get_bloginfo( 'name' )
                );
                the_social_share( $arg );
            }
        } else {
            die( "Social share helper functions (social.php) required!" );
        }
    }

    /**
     * Print The Post Date.
     *
     * @param string|null $format Custom PHP Date formating. Value null (default) uses Wordpress Format.
     */
    public function the_post_date( $format = null )
    {
        echo $this->get_post_date( $format );
    }

    /**
     * Get The Post Date.
     *
     * @param string|null $format Custom PHP Date formating. Value null (default) uses Wordpress Format.
     *
     * @return string Formated Post Date.
     */
    public function get_post_date( $format = null )
    {
        if ( ! isset( $format ) ) {
            $format = get_option( 'date_format' );
        }

        return date( $format, strtotime( $this->post_date ) );
    }

    /**
     * List specific taxonomy.
     *
     * @param string $taxonomy  Taxonomy name
     * @param string $seperator Seperator string, used to sperate each taxonomy.
     * @param string $field     Which field to use. Can use: term_id, name, slug,
     *
     * @return string Imploded taxonomy list glued with seperator.
     */
    public function list_taxonomy( $taxonomy = null, $seperator = ', ', $field = 'name' )
    {
        if ( ! isset( $taxonomy ) ) {
            return;
        }
        if ( isset( $taxonomy ) && isset( $this->categories[ $taxonomy ] ) ) {
            $cats = array();
            foreach ( $this->categories[ $taxonomy ] as $cat ) {
                $cats[] = $cat->$field;
            }
            echo implode( $seperator, $cats );
        }
    }
}