<?php
/**
 * Class used to setup configurable theme settings in Wordpress customizer.
 *
 * @todo Lots of stuff to complete
 * @author Gregor Sušanj, Združba IP, d.o.o.
 * @ver 1.0
 */

/**
 * Changelog:
 * ver 1.0 (24.3.2016):
 *      - Base version
 */



global $themeSettings;
$themeSettings = new ThemeSettings;

/**
 * Used to create settings page, manipulate it and access the options.
 *
 * Class ThemeSettings
 */
class ThemeSettings
{

    function  __construct()
    {
        // Add thumbnail and HTML5 support
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'html5', array( 'search-form' ) );

        // TODO: Check template image sizes
        //remove_image_size('medium');
        //remove_image_size('large');
        //add_image_size('svga', 800, 600, true);
        //add_image_size('sxga', 1280, 1024, true);
        //add_image_size('hd720', 1280, 720, true);
        //add_image_size('hd1080', 1920, 1080, true);


        // Add custom TinyMCE styling, based on sitewide styling
        add_editor_style( 'css/admin_editor_styles.min.css' );

        // Add menu support and register main menu
        if ( function_exists( 'register_nav_menus' ) ) {
            register_nav_menus(
                array(
                    'main_menu'   => 'Main Menu',
                    'footer_menu' => 'Footer Menu'
                )
            );
        }

        // Register actions and filters
        add_action( "customize_register", array( $this, "action_customize_register" ) );
        add_action( "customize_preview_init", array( $this, "action_customize_preview_init" ) );

        if ( is_admin() ) {
        } else {
            add_action( "wp_head", array( $this, "action_wp_head" ) );
        }

        // Translations
        if(function_exists("pll_register_string") && function_exists("pll_")){
            pll_register_string("zip_front_page", $this->get_option("front_page_quote"), "front_page", true);
        }
    }


    /**
     * Action hook 'customize_register'.
     * This hook allows you to define new Customizer panels, sections, settings, and controls.
     *
     * @url https://codex.wordpress.org/Plugin_API/Action_Reference/customize_register
     * @see https://codex.wordpress.org/Theme_Customization_API
     */
    public function action_customize_register( WP_Customize_Manager $wp_customize )
    {
        $wp_customize->add_setting("zip_theme_options[front_page_quote]", [
            'default'     => 'Razsvetli, osvobodi se lahko le človek, ki pride do tega stanja z mislimi.',
            'transport'   => 'refresh',
        ]);
        $wp_customize->add_section( 'zip_front_page' , array(
            'title'      => __( 'Nastavitve prednje strani', 'alt' ),
            'priority'   => 30,
        ) );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'front_page_quote', array(
            'label'        => __( 'Citat', 'alt' ),
            'section'    => 'zip_front_page',
            'settings'   => 'zip_theme_options[front_page_quote]',
        ) ) );

    }

    /**
     * Action hook 'wp_head'.
     * This hook allows you to output custom-generated CSS so that your changes show up correctly on the live website.
     *
     * The wp_head action hook is triggered within the <head></head> section of the user's
     * template by the wp_head() function.
     *
     * @url https://codex.wordpress.org/Plugin_API/Action_Reference/wp_head
     */
    public function action_wp_head()
    { ?>
        <style>

        </style>
        <script>

        </script>
        <?php
    }


    /**
     * Action hook 'customize_preview_init'.
     * This action hook allows you to enqueue assets (such as javascript files) directly in the Theme Customizer only.
     * To output saved settings onto your live site, you still need to output generated CSS using the wp_head hook.
     *
     * Generally, this hook is used almost exclusively to enqueue a theme-customizer.js file
     * for controlling live previews in WordPress's Theme Customizer.
     *
     * @url https://codex.wordpress.org/Plugin_API/Action_Reference/customize_preview_init
     */
    public function action_customize_preview_init()
    {
    }


    /**
     * Echoes the raw value of a theme option.
     *
     * @param string     $name
     * @param bool|false $default
     */
    public function the_option($name, $default = false){
        echo $this->get_option($name, $default);
    }

    /**
     * Gets the raw value of a theme option.
     *
     * @param string     $name
     * @param bool|false $default
     *
     * @return string
     */
    public function get_option($name, $default = false){
        $options = ( get_theme_mod( 'zip_theme_options' ) ) ? get_theme_mod( 'zip_theme_options' ) : null;

        // return the option if it exists
        if ( isset( $options[ $name ] ) ) {
            return apply_filters( 'zip_theme_options_$name', $options[ $name ] );
        }

        // return default if nothing else
        return apply_filters( 'zip_theme_options_$name', $default );
    }


    /**
     * Echoes translated theme option.
     *
     * @param string     $name
     * @param bool|false $default
     */
    public function the_option_translate($name, $default = false){
        if(function_exists("pll_e")){
            pll_e($this->get_option($name, $default));
        }else{
            $this->the_option($name, $default);
        }
    }

    /**
     * Gets translated theme option.
     *
     * @param string     $name
     * @param bool|false $default
     *
     * @return string
     */
    public function get_option_translate($name, $default = false){
        if(function_exists("pll__")){
            return pll__($this->get_option($name, $default));
        }else{
            return $this->get_option($name, $default);
        }
    }
}

