<?php
/**
 * Base class used to create custom post types.
 *
 * @author Gregor Sušanj, Združba IP, d.o.o.
 * @ver 1.0
 */

/**
 * Changelog:
 * ver 1.0 (24.3.2016):
 *      - Base version
 */

/**
 * Class CPTBase
 *
 * Used to setup basic things.
 */
abstract class CPTBase{

    /**
     * @var array Main custom post type arguments.
     */
    protected $cpt_args;
    /**
     * @var array List of Taxonomy arguments. Keys represent name of taxonomy.
     */
    protected $tax_args;

    /**
     * Base constructor to setup actions and hooks.
     */
    function __construct()
    {
        add_action( 'init', array( $this, "action_init" ) );
    }

    /**
     * Register custom post type and taxonomy if available.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     */
    public function action_init()
    {
        register_post_type( $this->cpt_args['name'], $this->cpt_args );

        if(!empty($this->tax_args)) {
            foreach ( $this->tax_args as $name => $tax_arg ) {
                register_taxonomy( $name, array( $this->cpt_args['name'] ), $tax_arg );
            }
        }
    }


}
