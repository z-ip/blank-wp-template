<?php
/**
 * Example class used to create custom post types.
 *
 * @author Gregor Sušanj, Združba IP, d.o.o.
 * @ver 1.0
 */

/**
 * Changelog:
 * ver 1.0 (24.3.2016):
 *      - Base version
 */


require_once "cptBase.php";

/**
 * Class CPTExample
 *
 * Used as copy base of creating new custom post types.
 *
 */
class CPTExample extends CPTBase
{

    /**
     * Setup all CPT and Taxonomy variables here.
     */
    function __construct()
    {
        $this->cpt_args = array(
            'name'                 => 'zip_example',
            'labels'               => array(
                'name'               => __( 'Custom item', 'zip' ),
                'singular_name'      => __( 'Item', 'zip' ),
                'menu_name'          => __( 'Custom items', 'zip' ),
                'name_admin_bar'     => __( 'Item', 'zip' ),
                'add_new'            => __( 'Add New', 'zip' ),
                'add_new_item'       => __( 'Add New Item', 'zip' ),
                'new_item'           => __( 'New Item', 'zip' ),
                'edit_item'          => __( 'Edit Item', 'zip' ),
                'view_item'          => __( 'View Item', 'zip' ),
                'all_items'          => __( 'All Items', 'zip' ),
                'search_items'       => __( 'Search Items', 'zip' ),
                'parent_item_colon'  => __( 'Parent Items:', 'zip' ),
                'not_found'          => __( 'No items found.', 'zip' ),
                'not_found_in_trash' => __( 'No items found in Trash.', 'zip' )
            ),
            'description'          => __( 'Custom Description.', 'zip' ),
            'public'               => true,
            'exclude_from_search'  => false,
            'publicly_queryable'   => true,
            'show_ui'              => true,
            'show_in_menu'         => true,
            'show_in_nav_menus'    => true,
            'show_in_admin_bar'    => true,
            'query_var'            => true,
            'rewrite'              => array(
                'slug'       => 'example',
                'with_front' => true,
                'feeds'      => true,
                // pages: Pagination
                'pages'      => true,
            ),
            'map_meta_cap'         => false,
            'capability_type'      => 'post',
            'has_archive'          => true,
            'hierarchical'         => false,
            // menu_position: Default null (at the bottom)
            'menu_position'        => null,
            // TODO: Can also use dashicons https://developer.wordpress.org/resource/dashicons/
            'menu_icon'            => get_template_directory_uri() . "/img/icon.png",
            'supports'             => array( 'title', 'editor', 'thumbnail' ),
            'register_meta_box_cb' => null,
            'can_export'           => true,
            'delete_with_user'     => false
        );

        /*
         * Taxonomies
         */
        $this->tax_args['zip_example_tag'] = array(
            'labels'                => array(
                'name'                       => __( 'Tags', 'zip' ),
                'singular_name'              => __( 'Tag', 'zip' ),
                'menu_name'                  => __( 'Tags', 'zip' ),
                'search_items'               => __( 'Search Tags', 'zip' ),
                'popular_items'              => __( 'Popular Tags', 'zip' ),
                'all_items'                  => __( 'All Tags', 'zip' ),
                'parent_item'                => __( 'Parent item', 'zip' ),
                'parent_item_colon'          => __( 'Parent item:', 'zip' ),
                'edit_item'                  => __( 'Edit Tag', 'zip' ),
                'update_item'                => __( 'Update Tag', 'zip' ),
                'add_new_item'               => __( 'Add new Tag', 'zip' ),
                'new_item_name'              => __( 'New Tag name', 'zip' ),
                'separate_items_with_commas' => __( 'Seperate items with commas.', 'zip' ),
                'add_or_remove_items'        => __( 'Add or remove Tags', 'zip' ),
                'choose_from_most_used'      => __( 'Choose from most used Tags', 'zip' ),
                'not_found'                  => __( 'Tag not found.', 'zip' )
            ),
            'description'           => __( 'Taxonomy description.', 'zip' ),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_nav_menus'     => true,
            'show_tagcloud'         => false,
            'show_in_quick_edit'    => true,
            'show_admin_column'     => true,
            'hierarchical'          => false,
            'update_count_callback' => '_update_post_term_count',
            'rewrite'               => array(
                'slug'         => 'example_tag',
                'with_front'   => true,
                'hierarchical' => false
            ),
            'sort'                  => false,
            'meta_box_cb'           => null
        );

        parent::__construct();
    }


}

new CPTExample();