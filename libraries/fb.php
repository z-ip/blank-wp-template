<?php
/**
 * A basic Facebook integration.
 *
 * How to use:
 * Create a client under instagram API and copy CLIENT_ID, SECRET and REDIRECT_URL.
 * Then follow instruction, that are printed with Instagram_Integration::get_token() function.
 *
 * @ver    1.1
 * @link   https://instagram.com/developer/
 * @source from Wordpress plugin custom-facebook-feed
 */

/**
 * Changelog:
 *
 * TODO for next version:
 *      - Clean up the code.
 *      - Internationalization
 *      - Do docComments
 *
 * ver 1.1:
 *      - Moved class constants to global constants with FACEBOOK_ prefix and in a new file, so this file can be easly
 *      updated.
 *      - Some code cleanup
 *
 * ver 1.0:
 *      Base version.
 *
 */


/**
 * Custom library for Facebook integration using GRAPH API v3
 */
class Facebook_Integration
{
    /**
     * Default Facebook API gateway.
     */
    const GATEWAY = 'https://graph.facebook.com/';

    /**
     * Gets a specific Facebook page timeline of last POST_COUNT posts.
     *
     * @return array The returned timeline entries.
     */
    public function getPageTimeline()
    {
        $transient = 'facebook_page_timeline_query_results' . FACEBOOK_TOTAL_COUNT . '_cache' . FACEBOOK_CACHE_TIME;

        //Get any existing copy of our transient data
        if ( false === ( $posts_json = get_transient( $transient ) ) ) {
            /* TODO: Less fields is more */
            $fields = array(
                'id',
                'from',
                'message',
                'message_tags',
                'story',
                'link',
                'attachments',
                'source',
                'name',
                'caption',
                'description',
                'type',
                'status_type',
                'object_id',
                'created_time',
                'properties'
            );
            $url    = self::GATEWAY . FACEBOOK_PAGE_ID . '/posts'
                      . '?fields=' . implode( ',', $fields )
                      . '&access_token=' . FACEBOOK_ACCESS_TOKEN
                      . '&limit=' . FACEBOOK_TOTAL_COUNT
                      . '&locale=' . FACEBOOK_LOCALE;
            if ( is_ssl() ) {
                $url .= '&return_ssl_resources=true';
            }

            $posts_json = $this->fetchData( $url );

            if ( ! empty( $posts_json ) ) {
                set_transient( $transient, $posts_json, FACEBOOK_CACHE_TIME * MINUTE_IN_SECONDS );
            }
        }

        $return  = array();
        $fb_data = json_decode( $posts_json );

        if ( isset( $fb_data, $fb_data->data ) && ! empty( $fb_data->data ) ) {
            foreach ( $fb_data->data as $p ) {
                $split_array = explode( '_', $p->id );
                $p->user_id  = $split_array[0];
                $p->post_id  = $split_array[1];

                // We only want our own posts
                if ( $p->from->id != $p->user_id ) {
                    continue;
                }

                if ( $p->type == 'photo' && $p->status_type == 'added_photos' ) {
                    continue;
                }
                $return[] = $p;
            }
        }

        return $return;
    }


    /**
     * Curl fetch function.
     *
     * @param $url string Url to fetch from.
     *
     * @return string json data or HTML returned from url.
     */
    private function fetchData( $url )
    {
        $feedData = null;

        //Can we use cURL?
        if ( is_callable( 'curl_init' ) ) {
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt( $ch, CURLOPT_TIMEOUT, 20 );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch, CURLOPT_ENCODING, '' );
            $feedData = curl_exec( $ch );
            curl_close( $ch );
            //If not then use file_get_contents
        } elseif ( ( ini_get( 'allow_url_fopen' ) == 1 || ini_get( 'allow_url_fopen' ) === true ) && in_array( 'https', stream_get_wrappers() ) ) {
            $feedData = @file_get_contents( $url );
            //Or else use the WP HTTP API
        } else {
            $request  = new WP_Http;
            $response = $request->request( $url, array( 'timeout' => 60, 'sslverify' => false ) );
            if ( is_wp_error( $response ) ) {
                //Don't display an error, just use the Server config Error Reference message
                $FBdata = null;
            } else {
                $feedData = wp_remote_retrieve_body( $response );
            }
        }

        return $feedData;
    }

}


//Make links into span instead when the post text is made clickable
function cff_wrap_span( $text )
{
    $pattern = '#\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))#';

    return preg_replace_callback( $pattern, 'cff_wrap_span_callback', $text );
}

function cff_wrap_span_callback( $matches )
{
    $max_url_length           = 100;
    $max_depth_if_over_length = 2;
    $ellipsis                 = '&hellip;';
    $target                   = 'target="_blank"';
    $url_full                 = $matches[0];
    $url_short                = '';
    if ( strlen( $url_full ) > $max_url_length ) {
        $parts           = parse_url( $url_full );
        $url_short       = $parts['scheme'] . '://' . preg_replace( '/^www\./', '', $parts['host'] ) . '/';
        $path_components = explode( '/', trim( $parts['path'], '/' ) );
        foreach ( $path_components as $dir ) {
            $url_string_components[] = $dir . '/';
        }
        if ( ! empty( $parts['query'] ) ) {
            $url_string_components[] = '?' . $parts['query'];
        }
        if ( ! empty( $parts['fragment'] ) ) {
            $url_string_components[] = '#' . $parts['fragment'];
        }
        for ( $k = 0; $k < count( $url_string_components ); $k ++ ) {
            $curr_component = $url_string_components[ $k ];
            if ( $k >= $max_depth_if_over_length || strlen( $url_short ) + strlen( $curr_component ) > $max_url_length ) {
                if ( $k == 0 && strlen( $url_short ) < $max_url_length ) {
                    // Always show a portion of first directory
                    $url_short .= substr( $curr_component, 0, $max_url_length - strlen( $url_short ) );
                }
                $url_short .= $ellipsis;
                break;
            }
            $url_short .= $curr_component;
        }
    } else {
        $url_short = $url_full;
    }

    return "<span class='cff-break-word'>$url_short</span>";
}

//2013-04-28T21:06:56+0000
//Time stamp function - used for posts
function cff_getdate( $original, $date_format, $custom_date = '' )
{
    switch ( $date_format ) {

        case '2':
            $print = date_i18n( 'F jS, g:i a', $original );
            break;
        case '3':
            $print = date_i18n( 'F jS', $original );
            break;
        case '4':
            $print = date_i18n( 'D F jS', $original );
            break;
        case '5':
            $print = date_i18n( 'l F jS', $original );
            break;
        case '6':
            $print = date_i18n( 'D M jS, Y', $original );
            break;
        case '7':
            $print = date_i18n( 'l F jS, Y', $original );
            break;
        case '8':
            $print = date_i18n( 'l F jS, Y - g:i a', $original );
            break;
        case '9':
            $print = date_i18n( "l M jS, 'y", $original );
            break;
        case '10':
            $print = date_i18n( 'm.d.y', $original );
            break;
        case '11':
            $print = date_i18n( 'm/d/y', $original );
            break;
        case '12':
            $print = date_i18n( 'd.m.y', $original );
            break;
        case '13':
            $print = date_i18n( 'd/m/y', $original );
            break;
        case '14':
            $print = date_i18n( 'j. n. Y', $original );
            break;
        default:

            $options = get_option( 'cff_style_settings' );

            $cff_second = isset( $options['cff_translate_second'] ) ? stripslashes( esc_attr( $options['cff_translate_second'] ) ) : '';
            if ( empty( $cff_second ) ) {
                $cff_second = 'second';
            }

            $cff_seconds = isset( $options['cff_translate_seconds'] ) ? stripslashes( esc_attr( $options['cff_translate_seconds'] ) ) : '';
            if ( empty( $cff_seconds ) ) {
                $cff_seconds = 'seconds';
            }

            $cff_minute = isset( $options['cff_translate_minute'] ) ? stripslashes( esc_attr( $options['cff_translate_minute'] ) ) : '';
            if ( empty( $cff_minute ) ) {
                $cff_minute = 'minute';
            }

            $cff_minutes = isset( $options['cff_translate_minutes'] ) ? stripslashes( esc_attr( $options['cff_translate_minutes'] ) ) : '';
            if ( empty( $cff_minutes ) ) {
                $cff_minutes = 'minutes';
            }

            $cff_hour = isset( $options['cff_translate_hour'] ) ? stripslashes( esc_attr( $options['cff_translate_hour'] ) ) : '';
            if ( empty( $cff_hour ) ) {
                $cff_hour = 'hour';
            }

            $cff_hours = isset( $options['cff_translate_hours'] ) ? stripslashes( esc_attr( $options['cff_translate_hours'] ) ) : '';
            if ( empty( $cff_hours ) ) {
                $cff_hours = 'hours';
            }

            $cff_day = isset( $options['cff_translate_day'] ) ? stripslashes( esc_attr( $options['cff_translate_day'] ) ) : '';
            if ( empty( $cff_day ) ) {
                $cff_day = 'day';
            }

            $cff_days = isset( $options['cff_translate_days'] ) ? stripslashes( esc_attr( $options['cff_translate_days'] ) ) : '';
            if ( empty( $cff_days ) ) {
                $cff_days = 'days';
            }

            $cff_week = isset( $options['cff_translate_week'] ) ? stripslashes( esc_attr( $options['cff_translate_week'] ) ) : '';
            if ( empty( $cff_week ) ) {
                $cff_week = 'week';
            }

            $cff_weeks = isset( $options['cff_translate_weeks'] ) ? stripslashes( esc_attr( $options['cff_translate_weeks'] ) ) : '';
            if ( empty( $cff_weeks ) ) {
                $cff_weeks = 'weeks';
            }

            $cff_month = isset( $options['cff_translate_month'] ) ? stripslashes( esc_attr( $options['cff_translate_month'] ) ) : '';
            if ( empty( $cff_month ) ) {
                $cff_month = 'month';
            }

            $cff_months = isset( $options['cff_translate_months'] ) ? stripslashes( esc_attr( $options['cff_translate_months'] ) ) : '';
            if ( empty( $cff_months ) ) {
                $cff_months = 'months';
            }

            $cff_year = isset( $options['cff_translate_year'] ) ? stripslashes( esc_attr( $options['cff_translate_year'] ) ) : '';
            if ( empty( $cff_year ) ) {
                $cff_year = 'year';
            }

            $cff_years = isset( $options['cff_translate_years'] ) ? stripslashes( esc_attr( $options['cff_translate_years'] ) ) : '';
            if ( empty( $cff_years ) ) {
                $cff_years = 'years';
            }

            $cff_ago = isset( $options['cff_translate_ago'] ) ? stripslashes( esc_attr( $options['cff_translate_ago'] ) ) : '';
            if ( empty( $cff_ago ) ) {
                $cff_ago = 'ago';
            }


            $periods        = array(
                $cff_second,
                $cff_minute,
                $cff_hour,
                $cff_day,
                $cff_week,
                $cff_month,
                $cff_year,
                "decade"
            );
            $periods_plural = array(
                $cff_seconds,
                $cff_minutes,
                $cff_hours,
                $cff_days,
                $cff_weeks,
                $cff_months,
                $cff_years,
                "decade"
            );

            $lengths = array( "60", "60", "24", "7", "4.35", "12", "10" );
            $now     = time();

            // is it future date or past date
            if ( $now > $original ) {
                $difference = $now - $original;
                $tense      = $cff_ago;
            } else {
                $difference = $original - $now;
                $tense      = $cff_ago;
            }
            for ( $j = 0; $difference >= $lengths[ $j ] && $j < count( $lengths ) - 1; $j ++ ) {
                $difference /= $lengths[ $j ];
            }

            $difference = round( $difference );

            if ( $difference != 1 ) {
                $periods[ $j ] = $periods_plural[ $j ];
            }
            $print = "$difference $periods[$j] {$tense}";

            break;
    }
    if ( ! empty( $custom_date ) ) {
        $print = date_i18n( $custom_date, $original );
    }

    return $print;
}

function cff_eventdate( $original, $date_format, $custom_date )
{
    switch ( $date_format ) {

        case '2':
            $print = date_i18n( 'F jS, g:ia', $original );
            break;
        case '3':
            $print = date_i18n( 'g:ia - F jS', $original );
            break;
        case '4':
            $print = date_i18n( 'g:ia, F jS', $original );
            break;
        case '5':
            $print = date_i18n( 'l F jS - g:ia', $original );
            break;
        case '6':
            $print = date_i18n( 'D M jS, Y, g:iA', $original );
            break;
        case '7':
            $print = date_i18n( 'l F jS, Y, g:iA', $original );
            break;
        case '8':
            $print = date_i18n( 'l F jS, Y - g:ia', $original );
            break;
        case '9':
            $print = date_i18n( "l M jS, 'y", $original );
            break;
        case '10':
            $print = date_i18n( 'm.d.y - g:iA', $original );
            break;
        case '11':
            $print = date_i18n( 'm/d/y, g:ia', $original );
            break;
        case '12':
            $print = date_i18n( 'd.m.y - g:iA', $original );
            break;
        case '13':
            $print = date_i18n( 'd/m/y, g:ia', $original );
            break;
        default:
            $print = date_i18n( 'F j, Y, g:ia', $original );
            break;
    }
    if ( ! empty( $custom_date ) ) {
        $print = date_i18n( $custom_date, $original );
    }

    return $print;
}

//Time stamp function - used for comments
function cff_timesince( $original )
{

    $options = get_option( 'cff_style_settings' );

    $cff_second = isset( $options['cff_translate_second'] ) ? stripslashes( esc_attr( $options['cff_translate_second'] ) ) : '';
    if ( empty( $cff_second ) ) {
        $cff_second = 'second';
    }

    $cff_seconds = isset( $options['cff_translate_seconds'] ) ? stripslashes( esc_attr( $options['cff_translate_seconds'] ) ) : '';
    if ( empty( $cff_seconds ) ) {
        $cff_seconds = 'seconds';
    }

    $cff_minute = isset( $options['cff_translate_minute'] ) ? stripslashes( esc_attr( $options['cff_translate_minute'] ) ) : '';
    if ( empty( $cff_minute ) ) {
        $cff_minute = 'minute';
    }

    $cff_minutes = isset( $options['cff_translate_minutes'] ) ? stripslashes( esc_attr( $options['cff_translate_minutes'] ) ) : '';
    if ( empty( $cff_minutes ) ) {
        $cff_minutes = 'minutes';
    }

    $cff_hour = isset( $options['cff_translate_hour'] ) ? stripslashes( esc_attr( $options['cff_translate_hour'] ) ) : '';
    if ( empty( $cff_hour ) ) {
        $cff_hour = 'hour';
    }

    $cff_hours = isset( $options['cff_translate_hours'] ) ? stripslashes( esc_attr( $options['cff_translate_hours'] ) ) : '';
    if ( empty( $cff_hours ) ) {
        $cff_hours = 'hours';
    }

    $cff_day = isset( $options['cff_translate_day'] ) ? stripslashes( esc_attr( $options['cff_translate_day'] ) ) : '';
    if ( empty( $cff_day ) ) {
        $cff_day = 'day';
    }

    $cff_days = isset( $options['cff_translate_days'] ) ? stripslashes( esc_attr( $options['cff_translate_days'] ) ) : '';
    if ( empty( $cff_days ) ) {
        $cff_days = 'days';
    }

    $cff_week = isset( $options['cff_translate_week'] ) ? stripslashes( esc_attr( $options['cff_translate_week'] ) ) : '';
    if ( empty( $cff_week ) ) {
        $cff_week = 'week';
    }

    $cff_weeks = isset( $options['cff_translate_weeks'] ) ? stripslashes( esc_attr( $options['cff_translate_weeks'] ) ) : '';
    if ( empty( $cff_weeks ) ) {
        $cff_weeks = 'weeks';
    }

    $cff_month = isset( $options['cff_translate_month'] ) ? stripslashes( esc_attr( $options['cff_translate_month'] ) ) : '';
    if ( empty( $cff_month ) ) {
        $cff_month = 'month';
    }

    $cff_months = isset( $options['cff_translate_months'] ) ? stripslashes( esc_attr( $options['cff_translate_months'] ) ) : '';
    if ( empty( $cff_months ) ) {
        $cff_months = 'months';
    }

    $cff_year = isset( $options['cff_translate_year'] ) ? stripslashes( esc_attr( $options['cff_translate_year'] ) ) : '';
    if ( empty( $cff_year ) ) {
        $cff_year = 'year';
    }

    $cff_years = isset( $options['cff_translate_years'] ) ? stripslashes( esc_attr( $options['cff_translate_years'] ) ) : '';
    if ( empty( $cff_years ) ) {
        $cff_years = 'years';
    }

    $cff_ago = isset( $options['cff_translate_ago'] ) ? stripslashes( esc_attr( $options['cff_translate_ago'] ) ) : '';
    if ( empty( $cff_ago ) ) {
        $cff_ago = 'ago';
    }


    $periods        = array(
        $cff_second,
        $cff_minute,
        $cff_hour,
        $cff_day,
        $cff_week,
        $cff_month,
        $cff_year,
        "decade"
    );
    $periods_plural = array(
        $cff_seconds,
        $cff_minutes,
        $cff_hours,
        $cff_days,
        $cff_weeks,
        $cff_months,
        $cff_years,
        "decade"
    );

    $lengths = array( "60", "60", "24", "7", "4.35", "12", "10" );
    $now     = time();

    // is it future date or past date
    if ( $now > $original ) {
        $difference = $now - $original;
        $tense      = $cff_ago;
    } else {
        $difference = $original - $now;
        $tense      = $cff_ago;
    }
    for ( $j = 0; $difference >= $lengths[ $j ] && $j < count( $lengths ) - 1; $j ++ ) {
        $difference /= $lengths[ $j ];
    }

    $difference = round( $difference );

    if ( $difference != 1 ) {
        $periods[ $j ] = $periods_plural[ $j ];
    }

    return "$difference $periods[$j] {$tense}";
}

//Use custom stripos function if it's not available (only available in PHP 5+)
if ( ! is_callable( 'stripos' ) ) {
    function stripos( $haystack, $needle )
    {
        return strpos( $haystack, stristr( $haystack, $needle ) );
    }
}
function cff_stripos_arr( $haystack, $needle )
{
    if ( ! is_array( $needle ) ) {
        $needle = array( $needle );
    }
    foreach ( $needle as $what ) {
        if ( ( $pos = stripos( $haystack, ltrim( $what ) ) ) !== false ) {
            return $pos;
        }
    }

    return false;
}

function cff_mb_substr_replace( $string, $replacement, $start, $length = null )
{
    if ( is_array( $string ) ) {
        $num = count( $string );
        // $replacement
        $replacement = is_array( $replacement ) ? array_slice( $replacement, 0, $num ) : array_pad( array( $replacement ), $num, $replacement );
        // $start
        if ( is_array( $start ) ) {
            $start = array_slice( $start, 0, $num );
            foreach ( $start as $key => $value ) {
                $start[ $key ] = is_int( $value ) ? $value : 0;
            }
        } else {
            $start = array_pad( array( $start ), $num, $start );
        }
        // $length
        if ( ! isset( $length ) ) {
            $length = array_fill( 0, $num, 0 );
        } elseif ( is_array( $length ) ) {
            $length = array_slice( $length, 0, $num );
            foreach ( $length as $key => $value ) {
                $length[ $key ] = isset( $value ) ? ( is_int( $value ) ? $value : $num ) : 0;
            }
        } else {
            $length = array_pad( array( $length ), $num, $length );
        }

        // Recursive call
        return array_map( __FUNCTION__, $string, $replacement, $start, $length );
    }
    preg_match_all( '/./us', (string) $string, $smatches );
    preg_match_all( '/./us', (string) $replacement, $rmatches );
    if ( $length === null ) {
        $length = mb_strlen( $string );
    }
    array_splice( $smatches[0], $start, $length, $rmatches[0] );

    return join( $smatches[0] );
}

//Push to assoc array
function cff_array_push_assoc( $array, $key, $value )
{
    $array[ $key ] = $value;

    return $array;
}

//Convert string to slug
function cff_to_slug( $string )
{
    return strtolower( trim( preg_replace( '/[^A-Za-z0-9-]+/', '-', $string ) ) );
}

// remove_filter( 'the_content', 'wpautop' );
// add_filter( 'the_content', 'wpautop', 99 );


//AUTOLINK
$GLOBALS['autolink_options'] = array(

    # Should http:// be visibly stripped from the front
    # of URLs?
    'strip_protocols' => true,

);

####################################################################

function cff_autolink( $text, $link_color = '', $span_tag = false, $limit = 100, $tagfill = 'class="cff-break-word"', $auto_title = true )
{

    $text = cff_autolink_do( $text, $link_color, '![a-z][a-z-]+://!i', $limit, $tagfill, $auto_title, $span_tag );
    $text = cff_autolink_do( $text, $link_color, '!(mailto|skype):!i', $limit, $tagfill, $auto_title, $span_tag );
    $text = cff_autolink_do( $text, $link_color, '!www\\.!i', $limit, $tagfill, $auto_title, 'http://', $span_tag );

    return $text;
}

####################################################################

function cff_autolink_do( $text, $link_color, $sub, $limit, $tagfill, $auto_title, $span_tag, $force_prefix = null )
{

    $text_l = StrToLower( $text );
    $cursor = 0;
    $loop   = 1;
    $buffer = '';

    while ( ( $cursor < strlen( $text ) ) && $loop ) {

        $ok      = 1;
        $matched = preg_match( $sub, $text_l, $m, PREG_OFFSET_CAPTURE, $cursor );

        if ( ! $matched ) {

            $loop = 0;
            $ok   = 0;
        } else {

            $pos     = $m[0][1];
            $sub_len = strlen( $m[0][0] );

            $pre_hit = substr( $text, $cursor, $pos - $cursor );
            $hit     = substr( $text, $pos, $sub_len );
            $pre     = substr( $text, 0, $pos );
            $post    = substr( $text, $pos + $sub_len );

            $fail_text = $pre_hit . $hit;
            $fail_len  = strlen( $fail_text );

            #
            # substring found - first check to see if we're inside a link tag already...
            #

            $bits     = preg_split( "!</a>!i", $pre );
            $last_bit = array_pop( $bits );
            if ( preg_match( "!<a\s!i", $last_bit ) ) {

                #echo "fail 1 at $cursor<br />\n";

                $ok = 0;
                $cursor += $fail_len;
                $buffer .= $fail_text;
            }
        }

        #
        # looks like a nice spot to autolink from - check the pre
        # to see if there was whitespace before this match
        #

        if ( $ok ) {

            if ( $pre ) {
                if ( ! preg_match( '![\s\(\[\{>]$!s', $pre ) ) {

                    #echo "fail 2 at $cursor ($pre)<br />\n";

                    $ok = 0;
                    $cursor += $fail_len;
                    $buffer .= $fail_text;
                }
            }
        }

        #
        # we want to autolink here - find the extent of the url
        #

        if ( $ok ) {
            if ( preg_match( '/^([a-z0-9\-\.\/\-_%~!?=,:;&+*#@\(\)\$]+)/i', $post, $matches ) ) {

                $url = $hit . $matches[1];

                $cursor += strlen( $url ) + strlen( $pre_hit );
                $buffer .= $pre_hit;

                $url = html_entity_decode( $url );


                #
                # remove trailing punctuation from url
                #

                while ( preg_match( '|[.,!;:?]$|', $url ) ) {
                    $url = substr( $url, 0, strlen( $url ) - 1 );
                    $cursor --;
                }
                foreach ( array( '()', '[]', '{}' ) as $pair ) {
                    $o = substr( $pair, 0, 1 );
                    $c = substr( $pair, 1, 1 );
                    if ( preg_match( "!^(\\$c|^)[^\\$o]+\\$c$!", $url ) ) {
                        $url = substr( $url, 0, strlen( $url ) - 1 );
                        $cursor --;
                    }
                }


                #
                # nice-i-fy url here
                #

                $link_url    = $url;
                $display_url = $url;

                if ( $force_prefix ) {
                    $link_url = $force_prefix . $link_url;
                }

                if ( $GLOBALS['autolink_options']['strip_protocols'] ) {
                    if ( preg_match( '!^(http|https)://!i', $display_url, $m ) ) {

                        $display_url = substr( $display_url, strlen( $m[1] ) + 3 );
                    }
                }

                $display_url = cff_autolink_label( $display_url, $limit );


                #
                # add the url
                #

                if ( $display_url != $link_url && ! preg_match( '@title=@msi', $tagfill ) && $auto_title ) {

                    $display_quoted = preg_quote( $display_url, '!' );

                    if ( ! preg_match( "!^(http|https)://{$display_quoted}$!i", $link_url ) ) {

                        $tagfill .= ' title="' . $link_url . '"';
                    }
                }

                $link_url_enc    = HtmlSpecialChars( $link_url );
                $display_url_enc = HtmlSpecialChars( $display_url );


                if ( substr( $link_url_enc, 0, 4 ) !== "http" ) {
                    $link_url_enc = 'http://' . $link_url_enc;
                }
                $buffer .= "<a href=\"{$link_url_enc}\">{$display_url_enc}</a>";
            } else {
                #echo "fail 3 at $cursor<br />\n";

                $ok = 0;
                $cursor += $fail_len;
                $buffer .= $fail_text;
            }
        }
    }

    #
    # add everything from the cursor to the end onto the buffer.
    #

    $buffer .= substr( $text, $cursor );

    return $buffer;
}

####################################################################

function cff_autolink_label( $text, $limit )
{

    if ( ! $limit ) {
        return $text;
    }

    if ( strlen( $text ) > $limit ) {
        return substr( $text, 0, $limit - 3 ) . '...';
    }

    return $text;
}

####################################################################

function cff_autolink_email( $text, $tagfill = '' )
{

    $atom = '[^()<>@,;:\\\\".\\[\\]\\x00-\\x20\\x7f]+'; # from RFC822

    #die($atom);

    $text_l = StrToLower( $text );
    $cursor = 0;
    $loop   = 1;
    $buffer = '';

    while ( ( $cursor < strlen( $text ) ) && $loop ) {

        #
        # find an '@' symbol
        #

        $ok  = 1;
        $pos = strpos( $text_l, '@', $cursor );

        if ( $pos === false ) {

            $loop = 0;
            $ok   = 0;
        } else {

            $pre  = substr( $text, $cursor, $pos - $cursor );
            $hit  = substr( $text, $pos, 1 );
            $post = substr( $text, $pos + 1 );

            $fail_text = $pre . $hit;
            $fail_len  = strlen( $fail_text );

            #die("$pre::$hit::$post::$fail_text");

            #
            # substring found - first check to see if we're inside a link tag already...
            #

            $bits     = preg_split( "!</a>!i", $pre );
            $last_bit = array_pop( $bits );
            if ( preg_match( "!<a\s!i", $last_bit ) ) {

                #echo "fail 1 at $cursor<br />\n";

                $ok = 0;
                $cursor += $fail_len;
                $buffer .= $fail_text;
            }
        }

        #
        # check backwards
        #

        if ( $ok ) {
            if ( preg_match( "!($atom(\.$atom)*)\$!", $pre, $matches ) ) {

                # move matched part of address into $hit

                $len  = strlen( $matches[1] );
                $plen = strlen( $pre );

                $hit = substr( $pre, $plen - $len ) . $hit;
                $pre = substr( $pre, 0, $plen - $len );
            } else {

                #echo "fail 2 at $cursor ($pre)<br />\n";

                $ok = 0;
                $cursor += $fail_len;
                $buffer .= $fail_text;
            }
        }

        #
        # check forwards
        #

        if ( $ok ) {
            if ( preg_match( "!^($atom(\.$atom)*)!", $post, $matches ) ) {

                # move matched part of address into $hit

                $len = strlen( $matches[1] );

                $hit .= substr( $post, 0, $len );
                $post = substr( $post, $len );
            } else {
                #echo "fail 3 at $cursor ($post)<br />\n";

                $ok = 0;
                $cursor += $fail_len;
                $buffer .= $fail_text;
            }
        }

        #
        # commit
        #

        if ( $ok ) {

            $cursor += strlen( $pre ) + strlen( $hit );
            $buffer .= $pre;
            $buffer .= "<a href=\"mailto:$hit\"$tagfill>$hit</a>";
        }
    }

    #
    # add everything from the cursor to the end onto the buffer.
    #

    $buffer .= substr( $text, $cursor );

    return $buffer;
}
