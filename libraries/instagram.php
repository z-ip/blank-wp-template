<?php
/**
 * A basic instagram integration.
 *
 * How to use:
 * Create a client under instagram API and copy CLIENT_ID, SECRET and REDIRECT_URL.
 * Then follow instruction, that are printed with Instagram_Integration::get_token() function.
 *
 * @ver  1.3
 * @link https://instagram.com/developer/
 */

/**
 * Changelog:
 *
 * TODO for next version:
 *      - Internationalization
 *      - CURL automation (get_token)
 *      - Remove Wordpress Depency
 *
 * ver 1.3:
 *      - Moved class constants to global constants with INSTAGRAM_ prefix and in a new file, so this file can be easly
 *      updated.
 *      - Added get_user_id which returns the ID from set username.
 *      - Transient api uses cache time and item count to easly update the cache.
 *      - Renamed get_token to instructions, as this are only instructions.
 *
 * ver 1.2:
 *      - Bugfixes
 *      - Upgraded automation (get_token)
 *      - Added support for PHP 5.3 by replacing if(empty(constant)) with if(constant == "")
 *
 * ver 1.1:
 *      - Uses class constants to define variables
 *      - Basic automation with get_token
 *
 * ver 1.0:
 *      Base version.
 *
 */


/**
 * Define this class or use its static methods.
 */
class Instagram_Integration
{
    /**
     * The default instagram gateway.
     * @link https://www.instagram.com/developer/endpoints/
     */
    const GATEWAY = "https://api.instagram.com/v1/";

    /**
     * Helper function to get access token for full API usage.
     *
     * Do not use empty(), because it does not support empty(const) before php 5.5. See note:
     * @link http://php.net/manual/en/function.empty.php
     */
    static function instructions()
    {
        if ( INSTAGRAM_CLIENT_ID != "" && INSTAGRAM_REDIRECT_URL != "" ) {
            $url1 = "https://instagram.com/oauth/authorize?client_id=" . INSTAGRAM_CLIENT_ID . "&redirect_uri=" . INSTAGRAM_REDIRECT_URL . "&response_type=code"; ?>
            <hr>
            <ol>
                <li>
                    Open <a href="<?php echo esc_url( $url1 ); ?>">link</a>, confirm and copy the "code" from url.
                </li>
                <li>
                    Get instagram <a href="http://jelled.com/instagram/lookup-user-id">user_ID</a> from client username.
                </li>

                <?php if ( INSTAGRAM_SECRET != "" && INSTAGRAM_CODE != "" ) : ?>
                    <li>
                        <label for="text_area">Use following curl command to get the ACCESS_TOKEN:</label>
					<textarea id="text_area">
curl -F 'client_id=<?php echo INSTAGRAM_CLIENT_ID; ?>' -F 'client_secret=<?php echo INSTAGRAM_SECRET; ?>' -F 'grant_type=authorization_code' -F 'redirect_uri=<?php echo INSTAGRAM_REDIRECT_URL; ?>
                        ' -F 'code=<?php echo INSTAGRAM_CODE; ?>' https://api.instagram.com/oauth/access_token
					</textarea>
                    </li>
                <?php else : ?>
                    <li>
                        To continue, type in the Secret, Code and User_ID constants and refresh the page.
                    </li>
                <?php endif; ?>
            </ol>
            <?php die( "<< End of instructions. >>" );
        } else {
            die( "Client ID and Redirect URL are empty!" );
        }
    }

    /**
     *
     */
    public function get_user_id()
    {
        $args     = array(
            "count"        => "1",
            "q"            => INSTAGRAM_USER_NAME,
            "access_token" => INSTAGRAM_ACCESS_TOKEN
        );
        $endpoint = "users/search";
        $url      = self::GATEWAY . $endpoint . "?" . $this->getArgs( $args );

        $json = json_decode( $this->fetchData( $url ) );
        var_dump( $json );
        die();
    }

    /**
     * Create url argument string.
     *
     * @param $args array Associated array of arguments.
     *
     * @return string & sign delimited string.
     */
    private function getArgs( $args )
    {
        $r = array();
        foreach ( $args as $key => $value ) {
            $r[ $key ] = $key . "=" . $value;
        }

        return implode( "&", $r );
    }

    /**
     * Curl fetch function.
     *
     * @param $url string Url to fetch from.
     *
     * @return string json data or HTML returned from url.
     */
    private function fetchData( $url )
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 20 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        $results = curl_exec( $ch );
        curl_close( $ch );

        return $results;
    }

    /**
     * Get images from supplied user_id.
     * API doc in link.
     *
     * @link https://instagram.com/developer/endpoints/users/#get_users_media_recent
     *
     * @return array List of images.
     */
    public function get_user_images()
    {
        $transient = 'instagram_user_query_results_count' . INSTAGRAM_COUNT . '_cache' . INSTAGRAM_CACHE_TIME;

        // Get any existing copy of our transient data
        if ( false === ( $json = get_transient( $transient ) ) ) {
            $args = array(
                "client_id"    => INSTAGRAM_CLIENT_ID,
                "count"        => INSTAGRAM_COUNT,
                "access_token" => INSTAGRAM_ACCESS_TOKEN
            );

            $endpoint = "users/" . INSTAGRAM_USER_ID . "/media/recent";
            $url      = self::GATEWAY . $endpoint . "?" . $this->getArgs( $args );

            $json = $this->fetchData( $url );

            if ( ! empty( $json ) ) {
                set_transient( $transient, $json, INSTAGRAM_CACHE_TIME * MINUTE_IN_SECONDS );
            }
        }


        if ( ! empty( $json ) ) {
            $res = json_decode( $json );
            if ( isset( $res->data ) ) {
                return $res->data;
            }
        }

        return array( "" );
    }


}
