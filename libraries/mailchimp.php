<?php

new Mailchimp_Integration();

/**
 * Mailchimp integration, used to subsrcibe people to Mailchimp account.
 *
 *
 * @todo GLOBAL: seperate JS from code, add standard styling
 */

/**
 * Mailchimp integration, used to subsrcibe people to Mailchimp account.
 *
 * How to use:
 * Execute Mailchimp_Integration::instructions(); and follow instructions. Make sure you use the function in a
 * development environment.
 *
 * @uses API 3.0
 * @ver  1.1
 */

/**
 * Changelog:
 *
 * TODO for next version:
 *      - Seperate JS from code
 *      - Add default styling
 *      - Remove Wordpress Depency
 *      - Lots of clean up
 *
 * ver 1.1:
 *      - Moved class constants to global constants with INSTAGRAM_ prefix and in a new file, so this file can be easly
 *      updated.
 *      - A bit of code cleanup
 *
 * ver 1.0:
 *      Base version.
 *
 */


/**
 * Define this class or use its static methods.
 */
class Mailchimp_Integration
{
    /**
     * constant string Mailchimp API v3.0 gateway.
     *
     * @todo: Make sure, the gateway uses the same server as the account
     */
    const GATEWAY = "https://" . MAILCHIMP_SUBDOMAIN . ".api.mailchimp.com/3.0/";


    /**
     * Setup Mailchimp subscribtion
     */
    function __construct()
    {
        if ( is_admin() ) {
            add_action( 'wp_ajax_mailchimpSubscribe', array( $this, 'doAjax' ) );
            add_action( 'wp_ajax_nopriv_mailchimpSubscribe', array( $this, "doAjax" ) );
        } else {
            switch ( MAILCHIMP_INTEGRATION_TYPE ) {
                case "AJAX":
                    add_action( 'wp_footer', array( $this, 'printJavascript' ), 21 );
                    break;
                case "POST":
                    if ( isset( $_POST["subscribe-form"] ) ) {
                        $this->doPost();
                    }
                    break;
            }
        }
    }

    /**
     * Run during POST request, after the form was submited.
     *
     * @todo: GLOBAL Clean up, integrate
     */
    private function doPost()
    {
        if ( isset( $_POST["nonce"], $_POST["firstname"], $_POST["lastname"], $_POST["email"] ) && wp_verify_nonce( $_POST["mc_nonce"], "mailchimp_subscribe_form" ) ) {
            if ( ! empty( $_POST["firstname"] ) && ! empty( $_POST["lastname"] ) && ! empty( $_POST["email"] ) && is_email( sanitize_email( $_POST["email"] ) ) ) {
                $firstname = sanitize_text_field( $_POST["firstname"] );
                $lastname  = sanitize_text_field( $_POST["lastname"] );
                $email     = sanitize_email( $_POST["email"] );

                // TODO: add interests the proper way
                $interests["interes"]["art"]    = isset( $_POST["art-soraj"] ) && $_POST["art-soraj"];
                $interests["interes"]["zeleni"] = isset( $_POST["zeleni-soraj"] ) && $_POST["zeleni-soraj"];
                $interests["interes"]["vital"]  = isset( $_POST["vital-soraj"] ) && $_POST["vital-soraj"];

                $return = $this->addSubscriber( $email, $firstname, $lastname, $interests );
                if ( $return == "success" ) {
                    die( "success" );
                } else {
                    die( "error" );
                }
            } else {
                die( "error" );
            }
        } else {
            die( "error" );
        }
    }

    /**
     * Subscribe a single user to CHIMP_LIST mailchimp list.
     *
     * @param $email      string Subscriber email.
     * @param $first_name string Subscriber first name.
     * @param $last_name  string Subscriber last name.
     *
     * @return string Wether it was a sucess, error or a user already exists.
     */
    public function addSubscriber( $email, $first_name = "", $last_name = "", $interests )
    {
        $endpoint = 'lists/' . MAILCHIMP_LIST . '/members/' . md5( strtolower( $email ) );

        $data = array(
            'apikey'        => MAILCHIMP_API_KEY,
            'email_address' => $email,
            'status'        => 'subscribed',
            'merge_fields'  => array(
                'FNAME' => $first_name,
                'LNAME' => $last_name
            )
        );

        // TODO: add interests the proper way

        $result = $this->fetchData( $endpoint, $data );


        if ( $result->status == 400 && $result->title == "Member Exists" ) {
            return "exists";
        } elseif ( $result->status == "subscribed" ) {
            return "success";
        } else {
            return "error";
        }
    }

    /**
     * Curl fetch function.
     *
     * @param $endpoint string Url endpoint to fetch from.
     * @param $data     Array|null Optional parameters for POST / PUT action.
     *
     * @return object json data or HTML returned from url.
     */
    public function fetchData( $endpoint, $data = null )
    {
        $ch = curl_init();

        if ( isset( $data ) ) {
            $json_data = json_encode( $data );
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "PUT" );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data );
        }
        curl_setopt( $ch, CURLOPT_URL, self::GATEWAY . $endpoint );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode( MAILCHIMP_USER . ':' . MAILCHIMP_API_KEY )
        ) );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0' );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );


        $result = json_decode( curl_exec( $ch ) );
        curl_close( $ch );

        return $result;
    }

    static function instructions()
    { ?>
        <hr>
        <ol>
            <li>
                Log in to desired <a href="http://www.mailchimp.com">Mailchimp</a> account and go to Profile / Extras /
                API keys
            </li>
            <li>Create a new key and copy the API key to social-variables.php</li>
            <li>After go to Lists /
                &gt;Desired List Name&lt;, after select Settings / List name and defaults
            </li>
            <li>Copy the List ID to social-variables.php</li>
            <li>
                Next step is typing in the server subodmain (which you can find in url, like <b>us11</b>.admin.mailchimp.com)
            </li>
            <li>
                The last step needs you to type the mailchimp user name to social-variables.php. Its the same as the
                Username you loged in.
            </li>
        </ol>
        <?php die( "<< End of instructions. >>" );
    }

    /**
     * Run during AJAX call, check and sanitize data then subscribe the user.
     */
    public function doAjax()
    {
        if ( ! isset( $_POST["email"] )
             && ( in_array( "name", MAILCHIMP_FIELDS ) && ! isset( $_POST["name"] ) )
             && ( in_array( "first_name", MAILCHIMP_FIELDS ) && ! isset( $_POST["first_name"] ) )
             && ( in_array( "last_name", MAILCHIMP_FIELDS ) && ! isset( $_POST["last_name"] ) )
        ) {
            echo "error";
        } else {
            $email      = sanitize_email( $_POST["email"] );
            $first_name = $last_name = "";
            if ( in_array( "name", MAILCHIMP_FIELDS ) ) {
                $first_name = sanitize_text_field( $_POST["name"] );
            }
            if ( in_array( "firstname", MAILCHIMP_FIELDS ) ) {
                $first_name = sanitize_text_field( $_POST["first_name"] );
            }
            if ( in_array( "lastname", MAILCHIMP_FIELDS ) ) {
                $last_name = sanitize_text_field( $_POST["last_name"] );
            }
            echo $this->addSubscriber( $email, $first_name, $last_name );
        }
        wp_die();
    }

    /**
     *  Print a default form for Mailchimp subscription.
     *
     * @todo: GLOBAL Make a popup
     */
    public function printForm()
    {
        if ( MAILCHIMP_INTEGRATION_TYPE == "POST" ) : ?>
            <form action="" method="post" role="form">
            <?php wp_nonce_field( "mailchimp_subscribe_form", 'mc_nonce' ); ?>
        <?php endif; ?>
        <input type="email" id="newsEmail" name="email" required
               placeholder="<?php echo esc_attr( $this->get_string( 'label_email' ) ); ?>" />
        <?php if ( in_array( "name", MAILCHIMP_FIELDS ) ) : ?>
        <input type="text" id="newsName" name="name" required
               placeholder="<?php echo esc_attr( $this->get_string( 'label_name' ) ); ?>" />
    <?php endif; ?>
        <?php if ( in_array( "firstname", MAILCHIMP_FIELDS ) ) : ?>
        <input type="text" id="newsFirstName" name="firstname" required
               placeholder="<?php echo esc_attr( $this->get_string( 'label_first_name' ) ); ?>" />
    <?php endif; ?>
        <?php if ( in_array( "lastname", MAILCHIMP_FIELDS ) ) : ?>
        <input type="text" id="newsLastName" name="lastname" required
               placeholder="<?php echo esc_attr( $this->get_string( 'label_last_name' ) ); ?>" />
    <?php endif; ?>
        <?php if ( MAILCHIMP_INTEGRATION_TYPE == "POST" ) : ?>
        <button type="submit" id="newsSubmit" name="subscribe-form" class=""></button>
    <?php elseif ( MAILCHIMP_INTEGRATION_TYPE == "AJAX" ) : ?>
        <button type="button" id="newsSubmit"></button>
    <?php endif; ?>
        <div id="newsResponse"></div>

        <?php if ( MAILCHIMP_INTEGRATION_TYPE == "POST" ) : ?>
        </form>
    <?php endif; ?>
        <?php
    }

    /**
     * Print a string in specified locale
     *
     * @param string      $str_name The string name to print
     * @param string|null $locale   optional The locale. Available: sl_SI and en.
     *
     * @return string The translated string
     */
    private function get_string( $str_name, $locale = null )
    {
        $available_locales = array( 'sl_SI', 'en' );
        if ( ! isset( $locale ) ) {
            $locale = get_locale();
        }
        if ( ! in_array( $locale, $available_locales ) ) {
            $locale = 'en';
        }

        $strings = array(
            'sl_SI' => array(
                'alert_all_fields_required' => "Vsa polja so obvezna.",
                'alert_invalid_email'       => "Vpisan e-poštni naslov je neveljaven.",
                'alert_success'             => "Prijava na novičarski seznam je uspešna.",
                'alert_duplicate'           => "Vpisani e-poštni naslov je že prijavljen.",
                'alert_general_error'       => "Med obdelavo je prišlo do napake. Poizkusite kasneje ponovno.",
                'alert_unknown_error'       => "Prišlo je do neznane napake. Ali imate vzpostavjeno internetno povezavo?",
                'label_email'               => "E-poštni naslov.",
                'label_name'                => "Ime in priimek.",
                'label_first_name'          => "Ime.",
                'label_last_name'           => "Priimek.",
                'label_submit'              => "Prijava"
            ),
            'en'    => array(
                'alert_all_fields_required' => "All fields are required.",
                'alert_invalid_email'       => "The provided email address is not valid.",
                'alert_success'             => "You have been successfully subscribed!",
                'alert_duplicate'           => "You are already subscribed to the mailing list!",
                'alert_general_error'       => "There was an error processing Your request. Please try again later.",
                'alert_unknown_error'       => "Unknown error occurred. Are You connected to the internet?",
                'label_email'               => "Your e-mail address.",
                'label_name'                => "Your first and last name.",
                'label_first_name'          => "Your first name.",
                'label_last_name'           => "Your last name.",
                'label_submit'              => "Submit"
            )
        );

        if ( isset( $strings[ $locale ][ $str_name ] ) ) {
            return $strings[ $locale ][ $str_name ];
        } else {
            return "Unknown string";
        }
    }

    /**
     *  Print javascript for ajax functionality.
     */
    public function printJavascript()
    {
        // TODO: properly name clases and IDs.
        ?>
        <script type="application/javascript">
            jQuery(document).ready(function ($) {
                var loading = false;
                $("#newsSubmit").click(function () {
                        $("#newsResponse").slideUp();
                        // First check if data is correct, then do AJAX
                        var email = $("#newsEmail");
                        <?php if ( in_array( "name", MAILCHIMP_FIELDS ) ) : ?>
                        var name = $("#newsName");
                        <?php endif; ?>
                        <?php if ( in_array( "firstname", MAILCHIMP_FIELDS ) ) : ?>
                        var firstname = $("#newsFirstName");
                        <?php endif; ?>
                        <?php if ( in_array( "lastname", MAILCHIMP_FIELDS ) ) : ?>
                        var lastname = $("#newsLastName");
                        <?php endif; ?>
                        if (!loading) {
                            if (email.val().length > 0
                                <?php if ( in_array( "name", MAILCHIMP_FIELDS ) ) : ?>
                                && name.val().length > 0
                                <?php endif; ?>
                                <?php if ( in_array( "firstname", MAILCHIMP_FIELDS ) ) : ?>
                                && firstname.val().length > 0
                                <?php endif; ?>
                                <?php if ( in_array( "lastname", MAILCHIMP_FIELDS ) ) : ?>
                                && lastname.val().length > 0
                                <?php endif; ?>
                            ) {
                                if (!validateEmail(email.val())) {
                                    alert('<?php echo $this->get_string('alert_invalid_email'); ?>');
                                    return false;
                                }
                                $.ajax({
                                        url: "<?php echo admin_url( 'admin-ajax.php' );?>",
                                        type: "POST",
                                        data: {
                                            action: "mailchimpSubscribe",
                                            email: email.val(),
                                            <?php if ( in_array( "name", MAILCHIMP_FIELDS ) ) : ?>
                                            name: name.val(),
                                            <?php endif; ?>
                                            <?php if ( in_array( "firstname", MAILCHIMP_FIELDS ) ) : ?>
                                            first_name: firstname.val(),
                                            <?php endif; ?>
                                            <?php if ( in_array( "lastname", MAILCHIMP_FIELDS ) ) : ?>
                                            last_name: lastname.val(),
                                            <?php endif; ?>
                                        },
                                        success: function (response) {
                                            if (response == "success") {
                                                displayNewsletterResponse('<span class="confirmation"><?php echo $this->get_string('alert_success'); ?></span>');
                                            } else if (response == "exists") {
                                                displayNewsletterResponse("<?php echo $this->get_string('alert_duplicate'); ?>");
                                            } else {
                                                displayNewsletterResponse("<?php echo $this->get_string('alert_general_error'); ?>");
                                            }
                                        },
                                        error: function () {
                                            displayNewsletterResponse("<?php echo $this->get_string('alert_unknown_error'); ?>");
                                        }
                                    }
                                );
                            }
                            else {
                                alert('<?php echo $this->get_string('alert_all_fields_required'); ?>');
                                return false;
                            }
                        }
                        return true;
                    }
                );

                function validateEmail(email) {
                    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                    return re.test(email);
                }

                function displayNewsletterResponse(text) {
                    var errorObject = $("#newsResponse");
                    errorObject.html(text);
                    errorObject.slideDown();
                }

            });
        </script>
        <?php
    }

    /**
     * Helper function, used to get IDs of interest categories and its items.
     *
     * @todo Clean up, make it more interactive
     */
    private function _getInterestCategories()
    {
        if ( MAILCHIMP_API_KEY == "" || MAILCHIMP_LIST == "" || MAILCHIMP_USER == "" ) {
            die( "Fill in the API_KEY, CHIMP_LIST and CHIM_USER class constants!" );
        }
        echo "<h3>First get all interest categories:</h3>";

        $endpoint = 'lists/' . MAILCHIMP_LIST . '/interest-categories';
        echo "<code>" . self::GATEWAY . $endpoint . "</code><br/>";

        $result = $this->fetchData( $endpoint );
        if ( isset( $result->categories ) && ! empty( $result->categories ) ) {
            echo "<p>Found " . count( $result->categories ) . " categories.</p>";
            foreach ( $result->categories as $cat ) {
                echo "<h4>Interest category '" . $cat->title . "' with ID: " . $cat->id . "</h4>";
                $endpoint = 'lists/' . MAILCHIMP_LIST . '/interest-categories/' . $cat->id . '/interests';
                echo "<code>" . self::GATEWAY . $endpoint . "</code><br/>";
                $result2 = $this->fetchData( $endpoint );
                if ( isset( $result2->interests ) && ! empty( $result2->interests ) ) {
                    echo "<p>Found " . count( $result2->interests ) . " items:</p>";
                    echo "<ol>";
                    foreach ( $result2->interests as $i => $interest ) {
                        echo "<li>'" . $interest->name . "' with ID: " . $interest->id . "</li>";
                    }
                    echo "</ol>";
                } else {
                    echo "<p>Interest category has no items.</p>";
                }
            }
        } else {
            echo "<p>No interest categories found.</p>";
        }
    }

}