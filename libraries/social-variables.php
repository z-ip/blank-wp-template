<?php
/**
 * Basic social configuration file. Change values in this file to allow smiple upgrade of social integrations.
 *
 * @author Gregor Sušanj, Združba IP, d.o.o.
 * @ver 1.2
 */

/**
 * Changelog:
 * ver 1.2 (24.3.2016):
 *      - Uses require_once instead of include_once
 *
 * ver 1.1:
 *      - Additional comments
 *
 * ver 1.0:
 *      - base version
 *      - moved all variable class constants to this file
 */



/******************************************
 **************** INSTAGRAM ***************
 ******************************************/

const INSTAGRAM_GATEWAY = "https://api.instagram.com/v1/";

const INSTAGRAM_CLIENT_ID = "";
const INSTAGRAM_SECRET = "";
const INSTAGRAM_REDIRECT_URL = "";
const INSTAGRAM_CODE = "";
const INSTAGRAM_USER_NAME = "";
const INSTAGRAM_USER_ID = "self";
const INSTAGRAM_ACCESS_TOKEN = "";

/**
 *  Number of images to get from the user.
 */
const INSTAGRAM_COUNT = 24;
const INSTAGRAM_CACHE_TIME = 15;

// require_once "instagram.php";

/*****************************************
 **************** FACEBOOK ***************
 *****************************************/

const FACEBOOK_USER_ID = "";
const FACEBOOK_PAGE_ID = "";
const FACEBOOK_ACCESS_TOKEN = "";

const FACEBOOK_LOCALE = "sl_SI";
const FACEBOOK_CACHE_TIME = 15;
const FACEBOOK_COUNT = 4;
const FACEBOOK_TOTAL_COUNT = 12;

// require_once "fb.php";

/****************************************
 **************** TWITTER ***************
 ****************************************/

const TWITTER_API_KEY = "";
const TWITTER_API_SECRET = "";
const TWITTER_ACCESS_TOKEN = "";
const TWITTER_ACCESS_TOKEN_SECRET = "";
const TWITTER_USER_ID = ""; // @link http://gettwitterid.com/

/**
 *  Number of Tweets to get.
 */
const TWITTER_COUNT = 4;
const TWITTER_CACHE_TIME = 15;

// require_once "twitter.php";

/******************************************
 **************** MAILCHIMP ***************
 ******************************************/

/* Make sure, the gateway uses the same server as the account */
const MAILCHIMP_SUBDOMAIN = "";
const MAILCHIMP_API_KEY = "";
const MAILCHIMP_USER = "";
const MAILCHIMP_LIST = ""; // Numeric ID
/* Use either AJAX or POST */
const MAILCHIMP_INTEGRATION_TYPE = "AJAX";

/* Input fields, used in Mailchimp integration. Simply remove array items to disable a feature */
const MAILCHIMP_FIELDS = array( "email", "name", "firstname", "lastname" );

/* If using interestes, set the catgory and item ID's. */
const MAILCHIMP_INTERESTS_CATEGORIES = array(
		array(
				'name'        => "",
				'category_id' => "",
				'items'       => array(
						'name'    => "",
						'item_id' => ""
				)
		)
);

// require_once "mailchimp.php";