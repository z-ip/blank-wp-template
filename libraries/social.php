<?php
/**
 * General social related functions like share link
 *
 * Change log:
 * ver 1.0:
 *  - Initial
 * ver 1.1 (22.2.2016):
 *  - Added missing og:image:height in og:image:width in Yoast SEO.
 */


/**
 * Returns a specific social share link based on arguments
 * Following parameters are available:
 *     'type' - Type of social share target, available: email, pinterest, facebook, twitter, linkedin and gplus.
 *     'content' - (Optional) The link content to print. Defaults to FontAwsome icon representation.
 *     'url' - (Used by all but twitter) The linked url.
 *     'image_url' - (Used by Pinterest) Url of the image, representing the post.
 *     'title' - (Used by LinkedIn, Tumblr and Email) The title of the content to share.
 *     'text' - (Used by Twitter, LinkedIn, Pinterest, Tumblr and Email) The content to share.
 *     'source' - (Used by LinkedIn) Optional source name, like website or application name.
 *
 * @param $args Array Function arguments.
 *
 * @todo : Text representation, internationalize it, Email?
 * @link http://www.sharelinkgenerator.com/
 * @return string The link.
 */
function get_social_share( $args )
{
    $url      = "";
    $defaults = array(
        'type'      => "",
        'content'   => "",
        'url'       => "",
        'image_url' => "",
        'title'     => "",
        'text'      => "",
        'source'    => ""
    );

    $args = wp_parse_args( $args, $defaults );

    if ( empty( $args['content'] ) ) {
        switch ( $args['type'] ) {
            case "email":
                $args['content'] = '<i class="fa fa-envelope"></i>';
                break;
            case "pinterest":
                $args['content'] = '<i class="fa fa-pinterest"></i>';
                break;
            case "facebook":
                $args['content'] = '<i class="fa fa-facebook"></i>';
                break;
            case "twitter":
                $args['content'] = '<i class="fa fa-twitter"></i>';
                break;
            case "linkedin":
                $args['content'] = '<i class="fa fa-linkedin"></i>';
                break;
            case "gplus":
                $args['content'] = '<i class="fa fa-google-plus"></i>';
                break;
            case "tumblr":
                $args['content'] = '<i class="fa fa-tumblr"></i>';
                break;
        }
    }

    switch ( $args['type'] ) {
        case "email":
            // TODO: Create Email link
            break;
        case "pinterest":
            $url = 'https://pinterest.com/pin/create/button/?url=' . urlencode( $args['url'] )
                   . '&media=' . urlencode( $args['image_url'] )
                   . '&description=' . urlencode( $args['text'] );
            break;
        case "facebook":
            $url = 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode( $args['url'] );
            break;
        case "twitter":
            $url = 'https://twitter.com/home?status=' . urlencode( $args['text'] );
            break;
        case "linkedin":
            /**
             * @link https://developer.linkedin.com/docs/share-on-linkedin
             */
            $url = 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode( $args['url'] )
                   . '&title=' . urlencode( $args['title'] )
                   . '&summary=' . urlencode( $args['text'] )
                   . '&source=' . urlencode( $args['source'] );
            break;
        case "gplus":
            $url = 'https://plus.google.com/share?url=' . urlencode( $args['url'] );
            break;
        case "tumblr":
            $url = 'http://www.tumblr.com/share/link?url=' . urlencode( $args['url'] )
                   . '&name=' . urlencode( $args['title'] )
                   . '&description=' . urlencode( $args['text'] );

            break;
        default:
            return "";
    }

    return '<a href="' . esc_url( $url ) . '" target="_blank">' . $args['content'] . '</a>';
}

/**
 * Print a specific social share link based on arguments
 * Following parameters are available:
 *     'type' - Type of social share target, available: email, pinterest, facebook, twitter, linkedin and gplus.
 *     'content' - (Optional) The link content to print. Defaults to FontAwsome icon representation.
 *     'url' - (Used by all but twitter) The linked url.
 *     'image_url' - (Used by Pinterest) Url of the image, representing the post.
 *     'title' - (Used by LinkedIn, Tumblr and Email) The title of the content to share.
 *     'text' - (Used by Twitter, LinkedIn, Pinterest, Tumblr and Email) The content to share.
 *     'source' - (Used by LinkedIn) Optional source name, like website or application name.
 *
 * @param $args Array Function arguments.
 */
function the_social_share( $args )
{
    echo get_social_share( $args );
}


/**
 * Temporary fix
 * OpenGraph add a og:image:width and og:image:height for FB async load of og:image issues
 *
 * https://github.com/Yoast/wordpress-seo/issues/2151
 *  https://developers.facebook.com/docs/sharing/webmasters/optimizing#cachingimages
 */
function WPSEO_OpenGraph_Image()
{
    global $wpseo_og;

    // will get a array with images
    $opengraph_images = new WPSEO_OpenGraph_Image( $wpseo_og->options );

    foreach ( $opengraph_images->get_images() as $img ) {
        // this block of code will first convert url of image to local path
        // for faster process of image sizes later
        $upload_dir = wp_upload_dir();
        $img_src    = str_replace( $upload_dir['url'], $upload_dir['path'], $img );
        $size       = getimagesize( $img_src );

        // display of this tags with Yoast SEO plugin
        $wpseo_og->og_tag( 'og:image:width', $size[0] );
        $wpseo_og->og_tag( 'og:image:height', $size[1] );
    }
}

if ( class_exists( 'WPSEO_OpenGraph_Image' ) ) {
    add_filter( "wpseo_opengraph", "WPSEO_OpenGraph_Image" );
}