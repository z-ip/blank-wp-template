<?php
/**
 *  A basic Twitter integration.
 *
 * How to use:
 * Execute Twitter_Integrations::instructions(); and follow instructions. Make sure you use the function in a
 * development environment.
 *
 * @ver  1.2
 * @link https://dev.twitter.com/overview/documentation
 * @link DEBUG: https://dev.twitter.com/rest/tools/console
 */

/**
 * Changelog:
 *
 * TODO for next version:
 *      - Internationalization
 *      - add instructions
 *
 * ver 1.2: (Still open)
 *      - Moved class constants to global constants with TWITTER_ prefix and in a new file, so this file can be easly
 *      updated.
 *      - Bugfixes
 *      - Add more phpDocs comments
 *      - Name standardization; cammelCase
 *      - Created basic version for instructions.
 *
 * ver 1.1:
 *      - Bugfixes
 *      - Transient api uses cache time and item count to easly update the cache.
 *
 * ver 1.0:
 *      Base version.
 *
 */

/**
 * Define this class or use its static methods.
 */
class Twitter_Integration
{

    const GATEWAY = "https://api.twitter.com/1.1/";

    /**
     * Helper function to get access token for full API usage.
     */
    static function instructions()
    { ?>
        <hr>
        <ol>
            <li>Go to <a href="https://apps.twitter.com/">Twitter application management</a> and create a new App.</li>
            <li>TODO: redo steps</li>
            <li></li>
        </ol>
        <p>
            You can debug your Twitter script <a href="https://dev.twitter.com/rest/tools/console">here</a>.
        </p>
        <?php die( "<< End of instructions. >>" );
    }

    /**
     * Get Tweets from supplied user_id.
     * API doc in link.
     *
     * @link https://dev.twitter.com/rest/reference/get/statuses/user_timeline
     *
     * @return array List of Tweets.
     */
    public function getUserTweets()
    {
        // Get any existing copy of our transient data
        $transient = 'twitter_user_query_results_count' . TWITTER_COUNT . '_cache' . TWITTER_CACHE_TIME;

        if ( false === ( $json = get_transient( $transient ) ) || $json === null ) {
            $args = array(
                "user_id"         => TWITTER_USER_ID,
                "count"           => TWITTER_COUNT,
                "trim_user"       => true,
                "exclude_replies" => true,
            );

            /**
             * @link https://dev.twitter.com/rest/reference/get/statuses/user_timeline
             */
            $url = self::GATEWAY . "statuses/user_timeline.json";

            $json = $this->fetchData( $url, $args );
            if ( ! empty( $json ) ) {
                set_transient( $transient, $json, TWITTER_CACHE_TIME * MINUTE_IN_SECONDS );
            }
        }

        if ( ! empty( $json ) ) {
            return json_decode( $json );
        }

        return array( "" );
    }

    /**
     * Curl fetch function.
     *
     * @param $url  string Gateway and access point.
     * @param $args array Additional parameters.
     *
     * @return string Resulting JSON.
     */
    private function fetchData( $url, $args )
    {

        $oauth = array(
            'oauth_consumer_key'     => TWITTER_API_KEY,
            'oauth_nonce'            => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token'            => TWITTER_ACCESS_TOKEN,
            'oauth_timestamp'        => time(),
            'oauth_version'          => '1.0'
        );

        //  merge request and oauth to one array
        $oauth = array_merge( $oauth, $args );

        //  do some magic
        $base_info                = $this->buildBaseString( $url, 'GET', $oauth );
        $composite_key            = rawurlencode( TWITTER_API_SECRET ) . '&' . rawurlencode( TWITTER_ACCESS_TOKEN_SECRET );
        $oauth_signature          = base64_encode( hash_hmac( 'sha1', $base_info, $composite_key, true ) );
        $oauth['oauth_signature'] = $oauth_signature;

        //  make request
        $header  = array( $this->buildAuthorizationHeader( $oauth ), 'Expect:' );
        $options = array(
            CURLOPT_HTTPHEADER     => $header,
            CURLOPT_HEADER         => false,
            CURLOPT_URL            => $url . "?" . http_build_query( $args ),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        );

        $feed = curl_init();
        curl_setopt_array( $feed, $options );
        $json = curl_exec( $feed );
        curl_close( $feed );

        return $json;
    }

    /**
     * Create and encode url argument string.
     *
     * @param $baseURI
     * @param $method
     * @param $params
     *
     * @return string
     */
    private function buildBaseString( $baseURI, $method, $params )
    {
        $r = array();
        ksort( $params );
        foreach ( $params as $key => $value ) {
            $r[] = "$key=" . rawurlencode( $value );
        }

        return $method . "&" . rawurlencode( $baseURI ) . '&' . rawurlencode( implode( '&', $r ) );
    }

    /**
     * Create an authorization header for curl.
     *
     * @param $oauth
     *
     * @return string
     */
    private function buildAuthorizationHeader( $oauth )
    {
        $r      = 'Authorization: OAuth ';
        $values = array();
        foreach ( $oauth as $key => $value ) {
            $values[] = "$key=\"" . rawurlencode( $value ) . "\"";
        }
        $r .= implode( ', ', $values );

        return $r;
    }

    /**
     * Autolink Tweets.
     *
     * @param $tweet object Single Tweet.
     *
     * @return string Resulting string.
     */
    public function autolinkText( $tweet )
    {
        $return = $tweet->text;

        $entities = array();

        if ( is_array( $tweet->entities->urls ) ) {
            foreach ( $tweet->entities->urls as $e ) {
                $temp["start"]       = $e->indices[0];
                $temp["end"]         = $e->indices[1];
                $temp["replacement"] = "<a href='" . $e->expanded_url . "' target='_blank'>" . $e->display_url . "</a>";
                $entities[]          = $temp;
            }
        }
        if ( is_array( $tweet->entities->user_mentions ) ) {
            foreach ( $tweet->entities->user_mentions as $e ) {
                $temp["start"]       = $e->indices[0];
                $temp["end"]         = $e->indices[1];
                $temp["replacement"] = "<a href='https://twitter.com/" . $e->screen_name . "' target='_blank'>@" . $e->screen_name . "</a>";
                $entities[]          = $temp;
            }
        }
        if ( is_array( $tweet->entities->hashtags ) ) {
            foreach ( $tweet->entities->hashtags as $e ) {
                $temp["start"]       = $e->indices[0];
                $temp["end"]         = $e->indices[1];
                $temp["replacement"] = "<a href='https://twitter.com/hashtag/" . $e->text . "?src=hash' target='_blank'>#" . $e->text . "</a>";
                $entities[]          = $temp;
            }
        }
        if ( ! empty( $tweet->entities->media ) ) {
            var_dump( $tweet->entities->media );
            foreach ( $tweet->entities->media as $e ) {
                $temp["start"]       = $e->indices[0];
                $temp["end"]         = $e->indices[1];
                $temp["replacement"] = "<a href='" . $e->url . "' target='_blank'>" . $e->url . "</a>";
                $entities[]          = $temp;
            }
        }

        usort( $entities, function ( $a, $b ) {
            return ( $b["start"] - $a["start"] );
        } );


        foreach ( $entities as $item ) {
            $return = $this->mb_substr_replace( $return, $item["replacement"], $item["start"], $item["end"] );
        }

        return ( $return );
    }

    /**
     * Custom MultiByte implementation for substr_replace.
     *
     * @param $string  string The string to search in.
     * @param $replace string The strin to replace in position.
     * @param $start   int Start position index.
     * @param $end     int End position index.
     *
     * @return string
     */
    private function mb_substr_replace( $string, $replace, $start, $end )
    {
        return mb_substr( $string, 0, $start ) . $replace . mb_substr( $string, $end );
    }

    /**
     * Get "x seconds ago" way of time representation.
     *
     * @param $time string strtotime convertible string.
     *
     * @return string Resulting conversion.
     */
    public function ago( $time )
    {
        $lengths = array( "60", "60", "24", "7", "4.35", "12", "10" );

        $now        = time();
        $difference = $now - $time;

        for ( $j = 0; $difference >= $lengths[ $j ] && $j < count( $lengths ) - 1; $j ++ ) {
            $difference /= $lengths[ $j ];
        }

        $difference = round( $difference );
        $p          = "";
        if ( $difference == 1 ) {
            switch ( $j ) {
                case 0:
                    $p = "sekundo";
                    break;
                case 1:
                    $p = "minuto";
                    break;
                case 2:
                    $p = "uro";
                    break;
                case 3:
                    $p = "dnevom";
                    break;
                case 4:
                    $p = "tednom";
                    break;
                case 5:
                    $p = "mesecem";
                    break;
                case 6:
                    $p = "letom";
                    break;
                case 7:
                    $p = "desetletjem";
                    break;
            }
        } elseif ( $difference == 2 ) {
            switch ( $j ) {
                case 0:
                    $p = "sekundama";
                    break;
                case 1:
                    $p = "minutama";
                    break;
                case 2:
                    $p = "urama";
                    break;
                case 3:
                    $p = "dnevoma";
                    break;
                case 4:
                    $p = "tednoma";
                    break;
                case 5:
                    $p = "mesecoma";
                    break;
                case 6:
                    $p = "letoma";
                    break;
                case 7:
                    $p = "desetletjema";
                    break;
            }
        } else {
            switch ( $j ) {
                case 0:
                    $p = "sekundami";
                    break;
                case 1:
                    $p = "minutami";
                    break;
                case 2:
                    $p = "urami";
                    break;
                case 3:
                    $p = "dnevi";
                    break;
                case 4:
                    $p = "tedni";
                    break;
                case 5:
                    $p = "meseci";
                    break;
                case 6:
                    $p = "leti";
                    break;
                case 7:
                    $p = "desetletji";
                    break;
            }
        }

        return "pred $difference $p";
    }

}
